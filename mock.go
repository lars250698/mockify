package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode"
)
import "strings"

var prevUppercase = false

func main() {
	if len(os.Args) > 1 {
		bytes := fromArg()
		bytes = mock(bytes)
		fmt.Println(string(bytes))
	} else {
		fromPipe()
	}
}

func mock(bytes []byte) []byte {
	for i := 0; i < len(bytes); i++ {
		if !isLetter(bytes[i]) {
			continue
		}
		if prevUppercase {
			bytes[i] = byte(unicode.ToLower(rune(bytes[i])))
			prevUppercase = false
		} else {
			bytes[i] = byte(unicode.ToUpper(rune(bytes[i])))
			prevUppercase = true
		}
	}

	return bytes
}

func isLetter(letter byte) bool {
	return (letter >= 'A' && letter <= 'Z') || (letter >= 'a' && letter <= 'z')
}

func fromArg() []byte {
	str := strings.Join(os.Args[1:], " ")
	bytes := []byte(str)
	return bytes
}

func fromPipe() {
	reader := bufio.NewReader(os.Stdin)
	chunk := make([]byte, 4)
	for {
		s, err := reader.Read(chunk)
		if s == 0 || err != nil {
			return
		}
		fmt.Print(string(mock(chunk)))
	}
}
